import random as random
import numpy as np
#Well, the goal is to use Monte-Carlo to guess the value of pi.
#I want to put a circle in a square, I know that much. What then?
#I would want to get some amount of points. Divide the number that land in the circle by the total number (those in the square) to get the ratio pi/4.
#How do I figure out whether one landed in the circle?
side = 1
radius = 0.5
center = [radius,radius]
#Try defining the circle. It's x^2 + y^2 = 1. The difficult thing is that I need to define x and y in terms of a third dummy variable.
def circle(u):
	x,y = u
	origin_x = center[0]
	origin_y = center[1]
	return (x - origin_x)**2 + (y - origin_y)**2 < radius**2
#Ok, they work. Or at least the origin points do.
circle_count = 0.
count = 10**7
for i in range(count):
	u = random.random(),random.random()
	if circle(u):
		circle_count += 1
#print "The number of points inside the circle is",circle_count,"."
pi = (circle_count / count) * 4
print "For",count,"iterations, the value of pi is",pi,"."
print "Increasing the number of iterations increases the accuracy of pi. By 100 iterations, pi is accurate in its first digit."
print "By 1000 iterations, pi is accurate to one decimal place."
print "By 10**5 iterations, pi is accurate to one decimal place, but is approaching 3.14."
print "It took me up to 10**7 iterations to achieve a value of pi accurate to three decimal places."
#Ok, looks like I'm all set. Clean up a little, save and push.