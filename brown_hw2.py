import numpy as np
print "PART 1: n=5"
n1=6
for b1 in range(n1):
	a1 = (3**-b1)
	x1 = np.float32(a1)
	y1 = np.float64(a1)
	print "At n=",b1,", x=",x1,". The double-precision value of this is ",y1,"."
	error1 = y1 - x1
	print "At n=",b1,", the absolute error is",error1,"." #The actual value y1 is a double precision number.
	relative_error1 = error1/y1
	print "At n=",b1,", the relative error is",relative_error1,"."
	
print "PART 2: n=20"
n2=21
for b2 in range(n2):
	a2 = (3**-b2)
	x2 = np.float32(a2)
	y2 = np.float64(a2)
	print "At n=",b2,", x=",x2,". The double-precision value of this is ",y2,"."
	error2 = y2 - x2
	print "At n=",b2,", the absolute error is",error2,"."
	relative_error2 = error2/y2
	print "At n=",b2,", the relative error is",relative_error2,"."
	
print "PART 3: x(1)=4, n=5"
#I have assumed that x=4 with the same equation x(n)=(1/3)^n.
n3=6
for b3 in range(n3):
	a3 = (3**-b3) + 3 + (1.5**-1)
	x3 = np.float32(a3)
	y3 = np.float64(a3)
	print "At n=",b3,", x=",x3,". The double-precision value of this is ",y3,"."
	error3 = y3 - x3
	print "At n=",b3,", the absolute error is",error3,"."
	relative_error3 = error3/y3
	print "At n=",b3,", the relative error is",relative_error3,"."
	
print "PART 4: x(1)=4, n=20"
n4=21
for b4 in range(n4):
	a4 = (3**-b4) + 3 + (1.5**-1)
	x4 = np.float32(a4)
	y4 = np.float64(a4)
	print "At n=",b4,", x=",x4,". The double-precision value of this is ",y4,"."
	error4 = y4 - x4
	print "At n=",b4,", the absolute error is",error4,"."
	relative_error4 = error4/y4
	print "At n=",b4,", the relative error is",relative_error4,"."

print "PART 5: x=4^n, n=20"
n5 = 21
for b5 in range(n5):
	a5 = (4**b5)
	x5 = np.float32(a5)
	y5 = np.float64(a5)
	print "At n=",b5,", x=",x5,". The double-precision value of this is ",y5,"."
	error5 = y5 - x5
	print "At n=",b5,", the absolute error is",error5,"."
	relative_error5 = error5/y5
	print "At n=",b5,", the relative error is",relative_error5,"."
print "The error for part 5 is zero because there are no fractions involved; the computer does not need to calculate infinite decimals. Problems should begin to arise if the answer gets too large for the computer to handle."

print "PART 6: TESTING LIMITS"
a6 = 3.4 * (10**38)
x6 = np.float32(a6)
y6 = np.float64(a6)
print "x=",x6,"."
print "y=",y6,"."
print "The computer can evaluate this. If we change the value to 3.41*10^38, though, we get the following:"
a7 = 3.41 * (10**38)
x7 = np.float32(a7)
y7 = np.float64(a7)
print "x=",x7,"."
print "y=",y7,"."
print "The computer cannot evaluate numbers above 3.4*10^38 as single-precision."