import random as random
import matplotlib.pyplot as plt
class Person():
	"""A person in the room. They can move, they can stand still, and they can be infected.
	Attributes:
		name: A string with the person's name.
		status: Is the person infected or not? 
		position: Location in the room."""
	def __init__(self,name='',status='alive'):
		self.name = name
		self.status = status

	def position1(self):
		old_x = random.random()
		old_y = random.random()
		position1 = [old_x,old_y]
		return position1
	def move(self):
		old_pos = Person.position1(self)
		motionx = random.random()
		motiony = random.random()
		motiontot = [motionx,motiony]
		motionneg = [-1*motionx,-1*motiony]
		#print motiontot
		#new_x = old_x + motionx
		#new_y = old_y + motiony
		newpos = map(sum,zip(old_pos,motiontot))
		limit = [1,1]
		for i in range(2):
			while newpos[i] > 1 or newpos[i] < 0:
				newmotion = [random.random(),random.random()]
				newpos[i] = old_pos[i] - newmotion[i]
		#newpos = new_x,new_y
		return newpos
Mary = Person('Mary','infected')
def infection_routine(x):
	infect_count = 0 #Because Mary will infect herself later.
	peoplelist = []
	peoplepos = []
	tol = 0.05 #How close two people can get before being infected.
	for n in range(x):
		person = Person()
		peoplelist.append(person)
		person_pos = person.move()
		peoplepos.append(person_pos)
		Mary_pos = peoplepos[0] #This should put Mary in the room.
		if abs(Mary_pos[0] - peoplepos[n][0]) <tol and abs(Mary_pos[1] - peoplepos[n][1]) <tol and person.status == 'alive':
			person.status = 'infected' #Mary will infect herself, but I can continue like that.
			infect_count += 1
		if n-1 >= 0:
			for p in range(n):
				if peoplelist[p].status == 'infected' and abs(peoplepos[p][0]-peoplepos[n][0]) < tol and abs(peoplepos[p][1] - peoplepos[n][1]) < tol and peoplelist[n].status == 'alive' and (p != 0):
					peoplelist[n].status = 'infected'
					infect_count += 1
	return infect_count
	print infect_count
infectlist = []
infect_count1 = 0 #This doesn't include Mary.
t = 0
tlist = []
infectsum = []
people = 10000
while sum(infectlist) < people:
	#infectlist.append(infection_routine(people - infect_count1))
	#print infectlist
	b = sum(infectlist)
	infectsum.append(b)
	c = people - int(b)
	#print "This iteration should involve",c,"people."
	infect_count1 = infection_routine(c)
	#print "This iteration infected",infect_count1,"people. The total number of people infected now is",(b + infect_count1),"."
	infectlist.append(infect_count1)
	t = t + 1
	tlist.append(t)
#print infectlist
#print infectsum
#print tlist
#The above code goes through the room and infects everyone over time. Now I need to know how long it takes.
#That's something. I know it's not what I'm looking for.
#I want to save the number of people infected in a single time step, remember that those people are infected, and run a new infection_routine with the remaining number of people alive.
plt.plot(tlist,infectsum)
plt.xlabel('Time')
plt.ylabel('People Infected')
plt.title('The Spread of Polio')
plt.show()
#print "Now we add in another type of status: Vaccinated. This can be done fairly simply; I could say that for n at some starting limit (say 50) until some ending limit, those people are vaccinated."
def infection_routine1(y,vaccines):
	infect_count2 = 0 #This is Mary. She is an addition to the people in the room (So if there are 100 people Mary makes it 101.)
	peoplelist1 = []
	peoplepos1 = []
	tol1 = abs(0.05 - 0.05*(vaccines/y)) #How close two people can get before being infected.
	for n in range(y):
		person1 = Person()
		peoplelist1.append(person1)
		person_pos1 = person1.move()
		peoplepos1.append(person_pos1)
	Mary_pos1 = peoplepos1[0] #This should put Mary in the room.
	for j in range(1,(vaccines+1)):
		peoplelist1[j].status = 'vaccinated'
	for n in range(y):	
		if Mary_pos1[0] - peoplepos1[n][0] <tol1 and Mary_pos1[1] - peoplepos1[n][1] <tol1 and peoplelist1[n].status == 'alive':
			peoplelist1[n].status = 'infected' #Mary will infect herself, but I can continue like that.
			infect_count2 += 1
		if n-1 >= 0:
			for p in range(n):
				if peoplelist1[p].status == 'infected' and abs(peoplepos1[p][0]-peoplepos1[n][0]) < tol1 and abs(peoplepos1[p][1] - peoplepos1[n][1]) < tol1 and peoplelist1[n].status == 'alive' and (p != 0):
					peoplelist1[n].status = 'infected'
					infect_count2 += 1
	return infect_count2
	#return Mary_pos1[0] - peoplepos1[0][0]
	#return abs(Mary_pos1[0] - peoplepos1[0][0])
infectlist1 = []
infect_count3 = 0
t1 = 0
tlist1 = []
infectsum1 = []
people1 = 10000
vaccines = 1000
while sum(infectlist1) < (people1 - vaccines):
	#infectlist.append(infection_routine(people - infect_count1))
	#print infectlist1
	b = sum(infectlist1)
	infectsum1.append(b)
	c = people1 - int(b)
	#print "This iteration should involve",c,"people."
	infect_count3 = infection_routine1(c,vaccines)
	#print "This iteration infected",infect_count3,"people. The total number of people infected now is",(b + infect_count3),"."
	infectlist1.append(infect_count3)
	t1 = t1 + 1
	tlist1.append(t1)
#print tlist1[-1]
#print infectsum1[-1]
plt.plot(tlist1,infectsum1)
plt.xlabel('Time')
plt.ylabel('People Infected')
plt.title('The Spread of Polio with Vaccinations')
plt.show()