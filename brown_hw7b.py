"""What is the smallest number of people in a room for the probability to be greater than 0.5 that two
people in the group have the same birthday?
I need to use a Monte-Carlo simulation to answer this question.
Because I need to match the month and day, I should use 365 days as birthdays.
If any two people in a sample size have the same value out of 365, they have the same birthday."""
import random
#Let's stop messing around and begin. I want to involve a range from 1-365 for birthdays.
people = 23 #Number of people in the room. Can be changed.
trialcount = 10**4 #Not the biggest sample size, but I'd like this to run fairly quickly.
sharecount = 0
for k in range(trialcount):
	bdaylist = []
	for i in range(people):
		bday = random.randrange(1,365,1)
		bdaylist.append(bday)
	sharefound = False
	for j in bdaylist:
		#print bdaylist.count(j) #That will go through each element of the birthday list previously produced and tell me how many times each element appears.
		if bdaylist.count(j) > 1:
			#print "Someone shares a birthday!"
			sharefound = True
	if sharefound == True:
		sharecount = sharecount + 1
	sharefound = 0
#print sharecount - That was just to make sure the ratio turns out ok.
ratio = float(sharecount) / float(trialcount)
percentage = 100 * ratio
print "In a room of",people,"people, there is a",percentage,"percent chance that two people will share a birthday."
print "In a room of 15 people, the chance of a shared birthday was about 25%. In a room of 35 people, the chance is about 80%."