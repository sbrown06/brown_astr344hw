#Start by defining and plotting functions.
import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-5, 5)
def f(x):
	return np.sin(x)
plt.plot(x,f(x))
plt.xlabel('x')
plt.ylabel('f(x)')
plt.title('f(x)=sin(x)')
plt.show()
def g(x):
	return (x**3) - x - 2
plt.plot(x,g(x))
plt.xlabel('x')
plt.ylabel('g(x)')
plt.title('g(x)=(x**3)-x-2')
plt.show()
def y(x):
	return -6 + x + (x**2)
plt.plot(x,y(x))
plt.xlabel('x')
plt.ylabel('y(x)')
plt.title('y(x)=-6+x+(x**2)')
plt.show()
#Offer a response string to ask for a number of iterations. If that's more than the default, use it.
#If the response is less than the default, use the default.
#First, pick some limits x1 and x2 and a step size between them. Make it so the user can set a smaller step size if they want.
x1 = -5
x2 = 5
print "Roots will be found on the interval from",x1,"to",x2,"."
hdef = 1 / (10.**6)
hres = 1 / (float(input("Please give a number of iterations. If your number is smaller than the default, the default will be used. ")))
if hres <= hdef and hres > 0:
	h = hres
else:
	h = hdef
print "This problem will use",(1 / h),"iterations."
#Next, set a default tolerance and a way to let the user change it.
toldef = 10.**-5
tolres = float(input("Please give a tolerance. If your tolerance is higher than the default, the default will be used. "))
if tolres <= toldef and tolres > 0:
	tol = tolres
else:
	tol = toldef
print "The tolerance for this problem is",tol,"units."

rangeused = np.arange(x1, (x2 + 1), h) #This will run through the interval from x1 to x2 with the specified number of iterations.
print "For f(x) = sin(x):"
frootfound = 0 #This is just a dummy variable that will make it so the same root is not reported multiple times where f(x) is within tolerance. I will be using a differently named variable for each function, just to be safe.
for xn in rangeused: 
	favg = (f(xn) + f(xn + h)) / 2
	if (-1 * tol) <= favg <= (tol):
		if frootfound == 0:
			fxroot = (xn + (xn + h))/2
			print "There is a root at ",fxroot,"."
			frootfound = 1
	else:
		frootfound = 0
#Consider setting something up such that if the start value is less than the end value, the program won't run. Otherwise, I'll get an overflow.
print "For g(x)=(x**3)-x-2:"
grootfound = 0
for xn in rangeused: 
	gavg = (g(xn) + g(xn + h)) / 2
	if (-1 * tol) <= gavg <= (tol):
		if grootfound == 0:
			gxroot = (xn + (xn + h))/2
			print "There is a root at ",gxroot,"."
			grootfound = 1
	else:
		grootfound = 0
		
print "For y(x)=-6+x+(x**2):"
yrootfound = 0
for xn in rangeused: 
	yavg = (y(xn) + y(xn + h)) / 2
	if (-1 * tol) <= yavg <= (tol):
		if yrootfound == 0:
			yxroot = (xn + (xn + h))/2
			print "There is a root at ",yxroot,"."
			yrootfound = 1
	else:
		yrootfound = 0

#To use this routine for any arbitrary function, define it with a letter and change the letters in the above for loops to match.