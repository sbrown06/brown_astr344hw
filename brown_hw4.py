#To begin, it would be good to define the functions involved. We start with H(z), since it's a part of the function r(z).
#I'm going to just plug in the values for the various omegas, since they are constant. Note that because Omega(r) is zero, I can eliminate that term.
C = 3*(10**3)
global z
def H(z):
	return ((0.3*(1 + z)**3) + (0*(1 + z)**2) + 0.7)**0.5
def r(zb):
	return C / H(zb)
#I have the independent variable of r(z) here as zb to avoid confusion between the two formulae. Similarly, when solving for the distance below, I use a third variable zc.
#If I test the above code by telling this to print some value of r(z), I get the same result that my calculator gives. The formula for r(z) works.
#Next, I set up the trapezoidal rule integrator.
#To begin, let's set up the variables: Start point, end point, and step size. Because step size = 1, I don't have to indicate it.
z1 = 0 #Starting point
z2 = 5 #Ending point + 1. I will be placing this at the upper end of a range.
h = 1 #I will still indicate the step size, in case I want to change it. This could make the for loop a little more complicated, depending on how much I'm overthinking things.
Atot = [] #A blank list, which will hold area values.
#Area of a trapezoid is ((a + b)/2) * h. Replace a and b with r(zn) and r(zn + h)
for zn in range(z1,(z2 + 1)):
	Atrap = 0.5 * h * (r(zn) + r(zn + h))
	Atot.append(Atrap)
#print Atot	
#I checked the code with a print(Atot) command, just to make sure things were working correctly. Atot is being stored as a list.
#Now, I need to sum all elements of Atot. I will assign this to a variable.
#Using a calculator to find the integral over the specified interval, the trapezoidal method is only off by a few hundred (about 7% error).
x = sum(Atot)
print "The numerical approximation of the integral is",x,"."
#Now that I have x equal to an approximation of the integral from z1 to z2 of r(z), I can plug this into the equation for D.

#I was having trouble before. I am writing zc as a numpy array that can be plotted with matplotlib instead of a regular list or a single value.
import numpy as np
zc = np.arange(z1,(z2 + 1)) #This gives individual sets of points (zc,Da(zc)).
def Da(zc):
	return x / (1 + zc)

import matplotlib.pyplot as plt
plt.plot(zc, Da(zc))
plt.xlabel("redshift")
plt.ylabel("distance")
plt.title('Numerical Approximation')
plt.show()
#Right now, the problem is that this function is not plotting as a function, but instead as a single point.
#The reason for that is that I have z set at a certain number at the top of this. Get z to initialize as a variable and I'll get a line.
#Below, I attempt to use np.trapz.
beta = np.array(Atot)
#print(beta)
alpha = np.trapz(beta)
print "The integral using np.trapz is",alpha,"."
plt.plot(zc, beta)
plt.xlabel('redshift')
plt.ylabel('distance')
plt.title('Integration using np.trapz')
plt.show()