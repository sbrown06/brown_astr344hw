import numpy as np
import math as math
import matplotlib.pyplot as plt
L = [1.1,2.3,3.4,4.6,5.7,6.9,8.0,9.1,10.3,11.5,12.6,13.8]
N = [8038,2676,773,354,200,110,65,41,27,13,6,2]
list_L = []
for item in L:
	list_L.append(float(item))
list_N = []
for item2 in N:
	list_N.append(float(item2))
alpha = []
beta = []
for b in range(11):
	numerator = list_N[(b + 1)]-list_N[b]
	denominator = list_L[(b+1)]-list_L[b]
	derivative = numerator / denominator
	#print "At n=",(b+1),", the derivative dN/dL is",derivative,"."
	a = float(list_L[b])
	alpha.append(math.log(a)) #The log of the luminosity.
	beta.append(math.log(-1 * derivative)) #The log of the derivative. There is a negative one in there because the log of a negative number doesn't exist.	
	#print "The coordinates at n=",b,"should be ",alpha,", ",beta,"."
#I think that plotting the log of the derivative versus the log of the luminosity should give a similar graph to the observed data.
#plt.plot(L,N)
#plt.xlabel('Luminosity')
#plt.ylabel('Number of galaxies')
#plt.title('Model data')
#plt.show() #This is going to show luminosity versus number of galaxies. If I take the log of this, it should look something like the observed data.
#plt.plot(alpha,beta)
#plt.xlabel('Log of luminosity')
#plt.ylabel('Log of number of galaxies')
#plt.title('Log of model data')
#plt.show()
#Just to check this against the observed data, I'm going to remove the log from the observed data and plot it against the raw data here.

Log_L = [-0.301030,0.00000,0.301030,0.602060,0.903090,0.778151,0.903090,1.00000,1.07918,0.00000,0.301030,0.477121,0.602060,0.778151,0.903090,-0.602060,-0.301030,0.00000,0.301030,0.602060,0.903090,1.20412,-0.856985,-0.619789,-0.526864,0.170951,0.192079,0.264071,0.376385,0.463049,0.578904,0.600342,0.940351,0.961730]
Log_N = [4.45025,3.72428,2.74036,2.24304,2.00000,2.06070,1.84510,1.84510,1.30103,3.91381,3.51055,3.24551,2.90309,2.50515,2.25527,4.98227,4.54407,3.81954,2.77815,2.30103,1.77815,1.41913,5.77073,5.56227,4.08065,4.30205,3.69701,3.28364,3.08834,2.77699,3.41157,1.99858,2.97109,2.66820]
for a in range(34):
	L2 = np.array((math.e**(Log_L[a])))
	N2 = np.array(math.e**(Log_N[a]))
#plt.plot(Log_L,Log_N)
#plt.xlabel('Log of luminosity')
#plt.ylabel('Log of number of galaxies')
#plt.title('Observed data')
#plt.show()

plt.figure(1)
plt.subplot(211)
plt.xlabel('Log of model luminosity')
plt.ylabel('Log of model # of galaxies')
plt.title('Model data')
plt.plot(alpha,beta)

plt.subplot(212)
plt.xlabel('Log of observed luminosity')
plt.ylabel('Log of observed # of galaxies')
plt.title('Observed data')
plt.plot(Log_L,Log_N)
plt.show()
