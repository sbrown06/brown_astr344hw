import numpy as np
import math
from datetime import datetime
from datetime import timedelta
def f1(x):
	return np.sin(x)
def f2(x):
	return (x**3) - x - 2
def f3(x):
	return -6 + x + (x**2)
#Next, figure out what my final N-R method should look like.
print "PART 1: NEWTON-RAPHSON"
#Here. The definition of a derivative is f(x+h)-f(x)/h, right? So I could do this centered at x instead of centered at x+h/2? To do that, I just want to say that df/dx = f(x+h)-f(x-h)/2h.
def derivative(f,x,step):
	return (f(x+step) - f(x-step)) / (2 * step) #This will create a centered derivative.
#Now I need to apply the above derivative to a loop.
def NR(f,x0,step):
	lastx = x0
	newx = lastx + 2 * step #I need it to be 2*step here because otherwise the while loop will terminate immediately.
	while abs(lastx - newx) >= step: #
		newvalue = f(newx)
		lastx = newx
		newx = lastx - (newvalue/derivative(f,lastx,step))
	return newx
xroot1 = NR(f1,2,10**-5)
xroot2 = NR(f2,1,10**-5)
xroot3 = NR(f3,0,10**-5)
print "For f(x)=sin(x), there is a root at x =",xroot1,"."
print "For f(x)=(x**3)-x-2, there is a root at x =",xroot2,"."
print "For f(x)=(x**2)+x-6, there is a root at x =",xroot3,"."
print ""
print "PART 2: TIMING"
print "Please note that I used my routine from HH5. Whether that is indeed bisection or not, I am unsure."
#Add in the things from HW5 that will be needed to perform this (I need tol, h, rangeused).
tol = 10.**-5
h = 10**-6 #Keep the same step size; otherwise it won't be fair.
range1 = np.arange(-5,0,h) #I have changed this from x1 and x2 to -5 and 5. In the interest of fairness, I will be starting N-R at -5 as well.
#Copy in the bisection routine for f(x).
print "For f(x)=sin(x) - HW5 method:"
f1rootfound = 0
t1f1 = datetime.now() #Time this all begins. Put commands between t1 and t2 to time them.
for xn in range1: 
	f1avg = (f1(xn) + f1(xn + h)) / 2
	if (-1 * tol) <= f1avg <= (tol):
		if f1rootfound == 0:
			f1xroot = (xn + (xn + h))/2
			print "There is a root at ",f1xroot,"."
			f1rootfound = 1
	else:
		f1rootfound = 0

t2f1 = datetime.now()
print "The HW5 method took",t2f1-t1f1,"seconds."
print "For f(x)=sin(x) - N-R method:"
t3f1 = datetime.now()
xroot4 = NR(f1,-5,h)
print "There is a root at",xroot4,"."
t4f1 = datetime.now()
print "The N-R method took",t4f1-t3f1,"seconds."
print"For f(x)=(x**3)-x-2 - HW5 method"
range2 = np.arange(1,2,h)
f2rootfound = 0
t1f2 = datetime.now()
for xn2 in range2: 
	f2avg = (f2(xn2) + f2(xn2 + h)) / 2
	if (-1 * tol) <= f2avg <= (tol):
		if f2rootfound == 0:
			f2xroot = (xn2 + (xn2 + h))/2
			print "There is a root at ",f2xroot,"."
			f2rootfound = 1
	else:
		f2rootfound = 0
t2f2 = datetime.now()
print "The HW5 method took",t2f2-t1f2,"seconds."
print "For f(x)=(x**3)-x-2 - N-R method:"
t3f2 = datetime.now()
xroot5 = NR(f2,1,h)
print "There is a root at",xroot5,"."
t4f2 = datetime.now()
print "The N-R method took",t4f2-t3f2,"seconds."
print "For f(x)=(x**2)+x-6 - HW5 method:"
range3 = np.arange(2,4,h)
f3rootfound = 0
t1f3 = datetime.now()
for xn3 in range3: 
	f3avg = (f3(xn3) + f3(xn3 + h)) / 2
	if (-1 * tol) <= f3avg <= (tol):
		if f3rootfound == 0:
			f3xroot = (xn3 + (xn3 + h))/2
			print "There is a root at ",f3xroot,"."
			f3rootfound = 1
	else:
		f3rootfound = 0
t2f3 = datetime.now()
print "The HW5 method took",t2f3-t1f3,"seconds."
print "For f(x)=(x**2)+x-6 - N-R method:"
t3f3 = datetime.now()
xroot6 = NR(f3,2,h)
t4f3 = datetime.now()
print "The N-R method took",t4f3-t3f3,"seconds."
print ""
print "PART 2: BLACKBODY RADIATION"
h = 6.6260755 * 10**-27 #units erg*s
c = 2.99792458 * 10**10 #units cm/s
k = 1.380658 * 10**-16 #units erg/K
B = 1.25 * 10**-12 #units ergs
wavelength = 870 * 10**-4 #units cm.
nu = c/wavelength #units 1/s
#I got the temperature in terms of intensity by hand.
def T(I):
	part1 = (h*nu)/k
	part2 = (2*h*(nu**3))/c**2
	temp = (part1) / math.log(1 + (part2 * 1/I))
	return temp
print "The temperature of a galaxy with blackbody radiation of intensity",B,"ergs and wavelength",wavelength,"centimeters is",T(B),"Kelvin. (I assume that the intensity is given in ergs.)"
#Ok. I have part 2 working just fine. Now I need to actually write the code for N-R.
