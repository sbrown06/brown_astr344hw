#Start by putting in the function.
def f(x):
	return (x**3) + 2 * (x**2) - 4
import numpy as np
#Now, set up limits of integration and step size.
print("I wasn't entirely sure what it meant for the step size to converge, so I took h=0.01.")
z1 = -1
z2 = 1
h = 0.01
arrange = np.arange(-1,1,0.01) #I was having trouble before with the program not accepting a float step size. Rewriting that range as a numpy array works, though.
print("PIECEWISE LINEAR INTEGRATION")
Atot1 = [] #A blank list to store areas of each rectangle.
for zm in arrange:
	Arec = h * f(zm)
	Atot1.append(Arec)
b = sum(Atot1)
print"The area under the curve using rectangles is",b,"units."
print("TRAPEZOIDAL METHOD")
Atot2 = [] #A blank list to store areas of each trapezoid.
for zn in arrange:
	Atrap = 0.5 * h * (f(zn) + f(zn + h))
	Atot2.append(Atrap)
y = sum(Atot2)
print'The area under the curve using the trapezoidal method is',y,'units.'
print('Using my calculator, I found the integral to be -6.666667 units.')
print"I did not have time to figure this out using Simpson's Rule or Gaussian Quadrature."
